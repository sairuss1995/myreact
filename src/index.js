import React from 'react';
import ReactDOM from 'react-dom';
import MyApp from './App';
import './style.css';

ReactDOM.render(
  <MyApp />,
  document.querySelector('#root')
);