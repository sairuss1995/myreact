import React,{Component} from "react";


class form extends Component{
    state = {
        password: '',
        login: '',
    }
    submitForm = event => {
        event.preventDefault();
        console.log(this.state)
        this.setState({password:'', login: '' })
        
    }
    setInput = ({target}) =>{this.setState({[target.name]: target.value})}
    render(){
        const {login, password } = this.state;
        const {children}= this.props;
         return(
        <form onSubmit={this.submitForm}>
            {children}
            <input 
            type="text" 
            placeholder="Login" 
            name="login"
            onChange={this.setInput}
            value={login}/>
            <input 
            type="password" 
            placeholder="Password" 
            name="password" 
            onChange={this.setInput}
            value={password}/>
            <button>send</button>

        </form>
    )
    }
   
}

export default form
