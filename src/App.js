import React, {Component} from "react";
import Form from "./componets/form";
//import Empty from "./componets/layouts/emptyLayout";
import Auth from "./componets/layouts/authLayout";

class app extends Component{
    state={
        authUser: {
          login: "Ivan"  
        }
    }
    setAuthUser = authUser => this.state({authUser})
    render(){
        const {authUser} = this.state;
        return(
       <Auth authUser={authUser}>
      <Form/>
  
      </Auth>
         )
    }
}
export default app;
    
     
