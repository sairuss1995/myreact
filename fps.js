class Car{
   constructor(name){
       this.name = name        
   }
        drive(){
            console.log(`${this.name} Is driving!`)
        }
}
const BMW = new Car('BMW');
const AUDI = new Car('AUDI');
const MERSEDES = new Car('MERSEDES');


BMW.drive();
AUDI.drive()
MERSEDES.drive();